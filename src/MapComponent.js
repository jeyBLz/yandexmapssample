import {FullscreenControl, ListBox, ListBoxItem, Map, ObjectManager, YMaps, ZoomControl} from 'react-yandex-maps';
import axios from 'axios';
import {useCallback, useEffect, useState} from 'react';

const specialties = ['Аналитик', 'Тестировщик', 'Разработчик'];
const mapOptions = {
  minZoom: 4,
  restrictMapArea: [[10, 10], [85, -160]]
};
const defaultMapState = {
  center: [55.751574, 37.573856],
  zoom: 4
};

export const MapComponent = () => {

  const [citiesInfo, setCitiesInfo] = useState([]);
  const [features, setFeatures] = useState([]);
  const [filters, setFilters] = useState(specialties);

  const addToFilters = useCallback((filter) => {
    if (!filters.includes(filter)) {
      setFilters(prev => [...prev, filter]);
    }
  }, [filters, setFilters]);

  const removeFromFilters = useCallback((filter) => {
    if (filters.includes(filter)) {
      setFilters(filters.filter(el => el !== filter));
    }
  }, [filters, setFilters]);

  useEffect(() => {
    axios.get('http://localhost:8080/mock')
      .then((res) => {
        setCitiesInfo(res.data);
        console.log('GET')
      })
  }, []);

  useEffect(() => {
    citiesInfo.forEach((city, i) => {
      city.employees.forEach((employee, j) => {
        setFeatures(prev => [...prev, {
          type: 'Feature',
          id: +`${i}${j}`,
          geometry: {
            type: 'Point',
            coordinates: city.coords
          },
          properties: {
            iconContent: 1
          },
          options: {
            preset: 'islands#blueCircleIcon'
          },
          specialty: employee.specialty
        }])
      })
    });
    console.log('SET')
    return () => {
      setFeatures([]);
    }
  }, [citiesInfo]);

  return (
    <YMaps>
      <Map
        width={'50vw'}
        height={'50vh'}
        defaultState={defaultMapState}
        defaultOptions={mapOptions}
      >
        <ZoomControl
          options={{
            zoomStep: 2
          }}
        />
        <FullscreenControl />
        <ListBox
          data={{
            content: 'Специализация'
          }}
        >
          {
            specialties.map(spec => {
              return <ListBoxItem
                key={spec}
                data={{
                  content: spec
                }}
                defaultState={{
                  selected: true
                }}
                onSelect={() => addToFilters(spec)}
                onDeselect={() => removeFromFilters(spec)}
              />
            })
          }
        </ListBox>
        <ObjectManager
          options={{
            clusterize: true
          }}
          filter={object => filters.includes(object.specialty)}
          defaultFeatures={features}
        />
      </Map>
    </YMaps>
  );
}
