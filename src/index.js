import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { MapComponent } from './MapComponent';

ReactDOM.render(
  <React.StrictMode>
    <MapComponent />
  </React.StrictMode>,
  document.getElementById('root')
);
